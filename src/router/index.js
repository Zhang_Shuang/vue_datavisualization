import Vue from 'vue'
import Router from 'vue-router'
//import Bar from '@/components/Bar'
import Dashboard from '@/components/Dashboard'
import chartOne from '@/charts/chartOne'
import chartTwo from '@/charts/chartTwo'
import chartThree from '@/charts/chartThree'
import chartFour from '@/charts/chartFour'

Vue.use(Router)

export default new Router({
    routes: [
        {
          path:'/',
          component:Dashboard,
          children:[
            {
              path:'/chartOne',
              component:chartOne
            },
            {
              path:'/chartTwo',
              component:chartTwo
            },
            {
              path:'/chartThree',
              component:chartThree
            },
            {
              path:'/chartFour',
              component:chartFour
            },
          ]
        },

  ]
})